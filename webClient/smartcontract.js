let web3;
let demoscratos;

function inscGouv(){
    let confForm = document.forms['conf-form'];
    let address = confForm['network-address'].value || 'http://localhost:5000';
    let ethAddress = confForm['eth-address'].value;
    let gouvPass = confForm['gouv-pass'].value;

    let dataJson='{"gouv-pass": "' + gouvPass + '", "eth-address": "'+ ethAddress +'"}'

    var request = new XMLHttpRequest()
    request.open('POST', 'http://127.0.0.1:5000/auth', false)

    request.setRequestHeader('Access-Control-Allow-Headers', '*');
    request.setRequestHeader('Content-type', 'application/json');
    request.setRequestHeader('Access-Control-Allow-Origin', '*');

    let content = document.getElementById("content");
    content.innerHTML="<h2>En attente de la réponse du gouvernement...</h2>"  

    request.send(dataJson)

    var data  = JSON.parse(request.response)
    if (data.code == 0){
	content.innerHTML="<h2>" + data.result +'</h2><a href="./vote.html"><button style="width:99%" class="btn" type="button"><span>Vote</span></button></a>'
    }else{
	content.innerHTML="<h2>" + data.result +"</h2>"  
    }

}


function checkContract() {
    let confForm = document.forms['conf-form'];
    let address = confForm['network-address'].value || 'http://localhost:8545';
    let contractAddress = confForm['contract-address'].value;

    web3 = new Web3(new Web3.providers.HttpProvider(address));

    if (!web3.utils.isAddress(contractAddress)) {
	document.getElementById('error-config').innerHTML = `Adresse ${contractAddress} invalide`;
	return;
    }

    web3.eth.getCode(contractAddress).then(res => {
	if (res == '0x') {
	    document.getElementById('error-config').innerHTML = `Contrat intelligent introuvable à l'adresse ${contractAddress}`;
	    return;
	}

	web3.eth.getAccounts().then(accounts => {
	    let accountsSelect = document.forms['account']['accounts'];

	    demoscratos = new web3.eth.Contract(JSON.parse(abi), contractAddress);

	    web3.eth.defaultAccount = accounts[0];

	    demoscratos.methods.getVoters().call().then(voters => {
		for (let voter of voters) {
		    let opt = document.createElement('option');
		    opt.text = voter;
		    opt.value = voter;
		    accountsSelect.appendChild(opt);
		}
		showStep(2);
	    });
	}, err => {
	    document.getElementById('error-config').innerHTML = `Réseau Ethereum <strong>${address}</strong> introuvable`;
	});
    });

}

function cipherVote(name){

    var request = new XMLHttpRequest()
    request.open('GET', 'http://127.0.0.1:5000/cle_public', false)

    request.setRequestHeader('Access-Control-Allow-Headers', '*');
    request.setRequestHeader('Content-type', 'application/json');
    request.setRequestHeader('Access-Control-Allow-Origin', '*');

    request.send()

    var data = JSON.parse(request.response)
    let cle = data.cle_pb;

    console.log(cle);

    var crypt = new JSEncrypt();

    crypt.setKey(cle);

    var text = '{"vote":"'+name+'"}';
    // Encrypt the data with the public key.
    let enc = crypt.encrypt(text);

    console.log(enc);

    return enc;	    

    /*	    crypt.setKey("-----BEGIN PUBLIC KEY-----\
	    MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQChbG9Gy9rFSd9ZILwVcTYWDM1R\
	    8T6BNQE30Pf56iPaoSxEr2Awrds2UidNRilkAMpi9zumnXsg4QRsKzCV6yoeXJmt\
	    n7GPCHBzqk3CSt+nnucrBgXj1Bimk+WQaWet0auwUxr8qr/+Bc1imrdrPW4WSUu9\
	    /Lej2V0mlox45wyVPQIDAQAB\
	    -----END PUBLIC KEY-----");


*/
    //	return enc;

}

function showStep(index) {
    for (let d of document.querySelectorAll('.step')) {
	d.style.display = 'none';
    }

    document.querySelector(`.step.s-${index}`).style.display = 'block';
}

function refreshTable() {
    let candidatesTable = document.getElementById('candidates');

    let hRow = document.createElement('tr');
    let actHCol = document.createElement('th');
    let candHCol = document.createElement('th');

    actHCol.innerHTML = 'Action';
    candHCol.innerHTML = 'Candidats';

    hRow.appendChild(actHCol);
    hRow.appendChild(candHCol);

    candidatesTable.innerHTML = '';
    candidatesTable.appendChild(hRow);

    document.getElementById('connected-as').innerHTML = `<span class="example">Connecté avec l'adresse <strong>${web3.eth.defaultAccount}<strong></span>`;

    demoscratos.methods.votedFor().call({ from: web3.eth.defaultAccount }).then(votedFor => {
	demoscratos.methods.getProposalNames().call().then(propNames => {
	    for (let [index, proposal] of propNames.entries()) {
		let row = document.createElement('tr');

		let cNameCol = document.createElement('td');
		let voteCountCol = document.createElement('td');
		let actCol = document.createElement('td');

		cNameCol.innerHTML = web3.utils.hexToAscii(proposal);
		/*        demoscratos.methods.getProposalVoteCount(index).call().then(nbVote => {
	  voteCountCol.innerHTML = `<span>${nbVote}</span>`;
	});*/




		console.log(votedFor)
		if (votedFor == 0x0) {
		    demoscratos.methods.endTimeStamp().call().then(endTime => {
			var d = new Date().valueOf();
			var epoch = d / 1000;
			console.log("EndTime : " + endTime)
			console.log("NowTime : " + epoch)
			if (endTime < epoch){
			    actCol.innerHTML = '<span>Le vote est terminé</span>';
			}else{
			    let actBtn = _createActButton();
			    let s = web3.utils.hexToAscii(proposal);
			    s=s.replace("\u0000","");
			    s.trim()
			    s=s.replace(/\0[\s\S]*$/g,'')
			    actBtn.setAttribute('onclick', `voteFor("${s}")`);
			    actCol.appendChild(actBtn);
			}
		    });

		}else {
		    actCol.innerHTML = '<span>Vous avez déjà voté</span>';

		}

		row.appendChild(actCol);
		row.appendChild(cNameCol);
		row.appendChild(voteCountCol);
		candidatesTable.appendChild(row);
	    }
	});
    });



    //=======================
    //  Résultat
    //=======================
    var request = new XMLHttpRequest()
    request.open('GET', 'http://127.0.0.1:5000/cle_privee', false)

    request.setRequestHeader('Access-Control-Allow-Headers', '*');
    request.setRequestHeader('Content-type', 'application/json');
    request.setRequestHeader('Access-Control-Allow-Origin', '*');

    request.send()

    var data = JSON.parse(request.response)
    let time = data.time;
    if(time == 0){
	let cle  = data.cle_pv;
	console.log(cle);

	var crypt = new JSEncrypt();
	crypt.setKey(cle);



	let resTable = document.getElementById('result');

	let hRow2 = document.createElement('tr');
	let actHCol2 = document.createElement('th');
	let candHCol2 = document.createElement('th');

	actHCol2.innerHTML = 'Vote chiffré';
	candHCol2.innerHTML = 'Vote déchiffré';

	hRow2.appendChild(actHCol2);
	hRow2.appendChild(candHCol2);

	resTable.innerHTML = '';
	resTable.appendChild(hRow2);
	demoscratos.methods.getNumberOfVote().call().then(nbVote=> {
	    for (var i=0; i < nbVote ; i++){
		demoscratos.methods.getVoteByIndex(i).call().then(data=> {
		    let row = document.createElement('tr');


		    let dataChiffreCol = document.createElement('td');
		    let dataDechiffreCol = document.createElement('td');
		    console.log(data)

		    let enc = crypt.decrypt(data);

		    dataChiffreCol.innerHTML = data;
		    dataDechiffreCol.innerHTML = enc;

		    row.appendChild(dataChiffreCol);
		    row.appendChild(dataDechiffreCol);
		    resTable.appendChild(row);
		});
	    }

	});
    }
}

function voteFor(proposalIndex) {
    let cipheredVote=cipherVote(proposalIndex)
    console.log(cipheredVote)
    demoscratos.methods.vote(cipheredVote).send({ from: web3.eth.defaultAccount }).then(res => {
	refreshTable();
    }, err => {
	document.getElementById('error-vote').innerHTML = 'Le vote a échoué';
    });
    alert("Vote envoyé ! \nVeuillez patienter quelques secondes.")
}

function unlockAccount() {
    let form = document.forms['account'];
    let accountAddress = form['accounts'].value;
    let password = form['acc-password'].value;
    web3.eth.personal.unlockAccount(accountAddress, password, function (error, valid) {
	if (!valid) {
	    document.getElementById('error-connect').innerHTML = 'Mot de passe invalide';
	    return;
	}

	web3.eth.defaultAccount = accountAddress;
	refreshTable();

	showStep(3);
    });
}

function _createActButton() {
    let actBtn = document.createElement('button');
    actBtn.setAttribute('class', 'btn');
    actBtn.innerHTML = '<span>Voter</span>';
    return actBtn;
}
