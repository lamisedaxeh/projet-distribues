//SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

contract Demoscratos {
  // This declares a new complex type which will
  // be used for variables later.
  // It will represent a single voter.
  struct Voter {
    bool voted;  // if true, that person already voted
  }
  
  string[] votes;// Valeur du vote

  //This is a type for a single proposal.
  struct Proposal {
    bytes32 name;   // short name (up to 32 bytes)
  }

  address public chairperson;
  uint256 public endTimeStamp;

  // This declares a state variable that
  // stores a `Voter` struct for each possible address.
  mapping(address => Voter) public voters;

  // A dynamically-sized array of `Proposal` structs.
  Proposal[] public proposals;
  bytes32[] public proposalNames;
  address[] public votersAddress;
  
  

  //Create new vote with 
  //pNames  (array of string)   : candidat:with
  //endTime (uint256)           : timestamp in s
  constructor(string[] memory pNames, uint256 endTime){
    chairperson = msg.sender;
    endTimeStamp = endTime;
    
    for (uint i = 0; i < pNames.length; i++) {
        proposals.push(Proposal({
            name: stringToBytes32(pNames[i])
        }));
        proposalNames.push(stringToBytes32(pNames[i]));
    }
  }

  //Only for the host
  //Give right to vote to address
  function giveRightToVote(address[] memory vs) public {
    //check if you are the hoster
    require(msg.sender == chairperson);
    //Give Right vote to all the array
    for (uint i = 0; i < vs.length; i++) {
        if (!voters[vs[i]].voted) {
            votersAddress.push(vs[i]);
        }
    }
  }
  
  // Get timestamp of blockchain  
  function getTime() public view returns (uint256) {
    uint256 chainStartTime = block.timestamp;
    return chainStartTime;
  }

  
  //Send vote
  //return a success boolean
  function vote(string memory proposal) public returns (bool){
    //check vote not ended
    if(block.timestamp < endTimeStamp){
        //check voter not already vote
        Voter storage sender = voters[msg.sender];
        require(!sender.voted);
        //set vote to true
        sender.voted = true;
        //send vote
        votes.push(proposal);
        return true;
    }
    return false;
  }

  // Return array of candidat
  function getProposalNames() public view returns (bytes32[] memory) {
      return proposalNames;
  }

  
  //Get number of vote
  function getNumberOfVote() public view returns (uint) {
    return votes.length;
  }
    
  //get vote form index of table votes[]
  function getVoteByIndex(uint index) public view returns (string memory) {
      if (index < votes.length){
          return votes[index];
      }
      return "";
  }

  // ruturns address array for votersAddress
  function getVoters() public view returns (address[] memory) {
      return votersAddress;
  }

  //Return true if you already vote  
  function votedFor() public view returns (bool) {
      Voter memory sender = voters[msg.sender];
      return sender.voted;
  }
  
  //covert string obj to byte[32] array
  function stringToBytes32(string memory source) public pure returns (bytes32 result) {
    bytes memory tempEmptyStringTest = bytes(source);
    if (tempEmptyStringTest.length == 0) {
        return 0x0;
    }
    assembly {
        result := mload(add(source, 32))
    }
  }
}
