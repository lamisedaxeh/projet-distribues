#!/usr/bin/env python
# encoding: utf-8
import json
import datetime
import time
from flask import Flask
from flask_cors import CORS
from flask import request, jsonify
app = Flask(__name__)
CORS(app)
from web3 import Web3
# Loding du json pour le smartcontract
abi = open("./contract-interface.json").read()
abi = json.loads(abi)

###################################
#  Config à modifier SmartContract
###################################
# URL du serveur ETH
w3 = Web3(Web3.HTTPProvider('http://127.0.0.1:8545'))
# ADRESSE du compte eth du serveur
w3.eth.defaultAccount = w3.toChecksumAddress("0x96d5e8d4d2a744d1db9338f5a4943cd09060a672")
print("Connected to ETH network : " + str(w3.isConnected()))
# ADDRESS du smartcontract
address = w3.toChecksumAddress("0xc4831E7047ca689D563035dc676A13FAe46FBF5d")
contract = w3.eth.contract(address=address, abi=abi)
###################################
#  Config à modifier Autre 
###################################
# CLE :
# Masupercléprivée
CLE_PRIVEE ="""-----BEGIN RSA PRIVATE KEY-----
MIICXQIBAAKBgQChbG9Gy9rFSd9ZILwVcTYWDM1R8T6BNQE30Pf56iPaoSxEr2Aw
rds2UidNRilkAMpi9zumnXsg4QRsKzCV6yoeXJmtn7GPCHBzqk3CSt+nnucrBgXj
1Bimk+WQaWet0auwUxr8qr/+Bc1imrdrPW4WSUu9/Lej2V0mlox45wyVPQIDAQAB
AoGAZCRSEDiNOtFuVuug4yh9pkZA5fHkleGdJNRl0ku3/Hz6ao3l/a0NjnB+40iA
iwKWi7jW77FI3ofZ54UPuiHO2VTdWcDZ3YIr+zr2YxFgJRqHEopEyxV6r7BF8ckp
l1dR69NEbc6q+yYsmzWCqpkqMwF8O8qPj5HVo3IibVYzlSECQQDLjnrGO/9yeYsg
Z2/5gB5Lt9v1kQYgiKChhHw5QeZ+BM4WX57F5LSX6xZopClvrzklrpNHZWT7kXzc
vtUfYro1AkEAywMVseznaqJ3/NidHDDhznh/9hwJuBXBVU6cUcZ1NRSRuMEG5L6d
BYnijQYXGFYLKa0QXvlcvCBefUmkQSgP6QJBALGM2bJHaw0izyO4lYb/JW3ONWIA
2WNPSS0ZLnGS9cU907TYOtlKH+vS9nzXk/0Czcn8eCJyYp67zDmPir4Rk3ECQQCQ
pIQCn05KtbNT3WojOdjI6SYSzetG04lKJJZP1VyREzS4NB+bPwxVc+aMn/g4iKWv
bfUqFfUyvMVV8vAq1EZxAkA9irFqCwOHQrTzg1OKj7aJTp1atCCRdX+DbWgZdArn
DG2MT2V/0kUhQJD9KWK8FCLMXpSFFSheNKJne5xgBJOQ
-----END RSA PRIVATE KEY-----"""
# Masuperclépublic
CLE_PUBLIC ="""-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQChbG9Gy9rFSd9ZILwVcTYWDM1R
8T6BNQE30Pf56iPaoSxEr2Awrds2UidNRilkAMpi9zumnXsg4QRsKzCV6yoeXJmt
n7GPCHBzqk3CSt+nnucrBgXj1Bimk+WQaWet0auwUxr8qr/+Bc1imrdrPW4WSUu9
/Lej2V0mlox45wyVPQIDAQAB
-----END PUBLIC KEY-----"""
# 
dateEtHeureLimiteDelection = datetime.datetime(year=2020,month=1,day=2,hour=9,minute=30,second=00)
publicationDeLaClePrive = True

@app.route('/cle_privee')
def cle_pv():
    if publicationDeLaClePrive and dateEtHeureLimiteDelection < datetime.datetime.now():
        response = app.response_class(
            response=json.dumps({'cle_pv': CLE_PRIVEE,
                                 'time':0
                                }),
            status=200,
            mimetype='application/json'

        )
    else:
        response = app.response_class(
            response=json.dumps({'cle_pv': "Veuillez patienter ",
                                 'time': (dateEtHeureLimiteDelection - datetime.datetime.now()).total_seconds()
                                }),
            status=200,
            mimetype='application/json'
        )
    return response
    
@app.route('/cle_public')
def cle_pb():
    response = app.response_class(
        response=json.dumps({'cle_pb': CLE_PUBLIC}),
        status=200,
        mimetype='application/json'
    )
    return response

@app.route('/getcandidat')
def getcandidat():
    response = app.response_class(
        response=json.dumps({"candidat":[{'nom': "tintin"},{'nom': "Milou"},{'nom': "professeur tournesol"}]}),
        status=200,
        mimetype='application/json'
    )
    return response


@app.route('/auth' , methods = ['POST'])
def auth():
    #Get data from post in json
    # ex : {'id': 'Alice', 'data': 'test'}
    post_data = request.get_json()
    # Open BDD file in json
    f=open('data.json','r').read()
    BDDinJson = json.loads(f)

    #Search for match
    match=list(filter(lambda x:x["gouv-pass"]==post_data['gouv-pass'],BDDinJson['user']))

    print(post_data)

    if len(match)==1:
        if  match[0]['dejaVote'] == 0:
            #if the person can vote
            #TODO call smart contract

            tx_hash = contract.functions.giveRightToVote([ w3.toChecksumAddress(post_data['eth-address']) ]).transact()
            w3.eth.waitForTransactionReceipt(tx_hash)
            response = app.response_class(
                response=json.dumps({'result':"Vous avez été ajouté à la liste électorale. Vous pouvez aller voter",'code':0}),
                status=200,
                mimetype='application/json'
            )
            match[0]['dejaVote']=1
            open('data.json','w').write(json.dumps(BDDinJson))
        else :
        # if the person has already voted
            response = app.response_class(
                response=json.dumps({'result':"Vous êtes déjà inscrit sur la liste électorale !",'code':1}),
                status=200,
                mimetype='application/json'
            )
    else:
        response = app.response_class(
            response=json.dumps({'result':"Votre mot de passe est invalide",'code':2}),
            status=200,
            mimetype='application/json'
        )


    return response

@app.route('/')
def index():
    response ="""
    <h1>List of route :</h1>
    <h2>Get candidat</h2>
    <a href="/getcandidat">http://localhost:5000/getcandidat</a>
    <h2>Get public key</h2>
    <a href="/cle_public">http://localhost:5000/cle_public</a>
    <h2 >Get private key if is released</h2>
    <a href="/cle_privee">http://localhost:5000/cle_privee</a>
    <h2>POST in Auth</h2>
    <a href="/auth">http://localhost:5000/auth</a>
    <h3>This root need json body with id example:</h3>
    <ul>
        <li>{'id': 'Alice', 'data': 'test'}</li>
    </ul>
    """
    return response

app.run()
